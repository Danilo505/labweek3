package com.example.music.repository;

import com.example.music.models.ProductMusic;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface MusicRepository extends JpaRepository<ProductMusic, Long> {
    Optional<List<ProductMusic>> findByArtist (String artist);
    Optional<List<ProductMusic>> findByTitle (String title);
    Optional<List<ProductMusic>> findByGenre (String genre);
}
