package com.example.music.models;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

public record RequestMusic (
        Long id,
        @NotBlank
        String title,
        @NotNull
        String artist,
        String genre,
        Double duration) {

}
