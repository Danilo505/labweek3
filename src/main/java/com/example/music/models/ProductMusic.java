package com.example.music.models;

import jakarta.persistence.*;
import lombok.*;




@Table(name = "music")
@Entity(name = "music")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(of = "id")
public class ProductMusic {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private Long id;
    private String title;
    private String artist;
    private String genre;
    private Double duration;

    public ProductMusic(RequestMusic music){
        this.title = music.title();
        this.artist = music.artist();
        this.genre = music.genre();
        this.duration = music.duration();
    }
}
