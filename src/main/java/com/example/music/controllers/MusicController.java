package com.example.music.controllers;

import com.example.music.models.ProductMusic;
import com.example.music.models.RequestMusic;
import com.example.music.repository.MusicRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/music")
public class MusicController {

    @Autowired
    private MusicRepository musicRepository;

    @GetMapping("/all")
    public ResponseEntity<List<ProductMusic>> getAllMusic(){
        List<ProductMusic> allMusic = musicRepository.findAll();
        return ResponseEntity.ok(allMusic);
    }

    @PostMapping
    public ResponseEntity<ProductMusic> registerMusic(@RequestBody @Validated RequestMusic data){
        var productmusic = new ProductMusic(data);
        musicRepository.save(productmusic);
        System.out.println(data);
        return ResponseEntity.status(HttpStatus.CREATED).body(productmusic);
    }

    @PutMapping("/{id}")
    public ResponseEntity<ProductMusic> updateMusic(@PathVariable Long id, @RequestBody @Validated RequestMusic data){
       ProductMusic music = musicRepository.findById(id).orElseThrow();
       music.setTitle(data.title());
       music.setArtist(data.artist());
       music.setGenre(data.genre());
       music.setDuration(data.duration());

       musicRepository.save(music);
       return ResponseEntity.ok(music);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ProductMusic> deleteMusic(@PathVariable long id){
        musicRepository.deleteById(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @GetMapping("/artist/{artist}")
    public ResponseEntity<List<ProductMusic>> getByArtistProductMusic (@PathVariable String artist){
        List<ProductMusic> productMusic = musicRepository.findByArtist(artist).orElseThrow();
        return ResponseEntity.ok(productMusic);

    }

    @GetMapping("/title/{title}")
    public ResponseEntity<List<ProductMusic>>  getByTitle (@PathVariable String title){
        List<ProductMusic> productMusic = musicRepository.findByTitle(title).orElseThrow();
        return ResponseEntity.ok(productMusic);
    }

    @GetMapping("/genre/{genre}")
    public ResponseEntity<List<ProductMusic>>  getByGenre (@PathVariable String genre){
        List<ProductMusic> productMusic = musicRepository.findByGenre(genre).orElseThrow();
        return ResponseEntity.ok(productMusic);
    }
}
